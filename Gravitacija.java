import java.util.Scanner;
import java.lang.Math;

public class Gravitacija {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        double v = sc.nextDouble() * 1000;
        
        double c = 6.674 * (Math.pow(10, -11));
        
        double m = 5.972 * (Math.pow(10, 24));
        
        double r = 6.371 * (Math.pow(10, 6));
        
        double a = (c * m) / (Math.pow((r + v), 2));
        
        System.out.println(a);
    }
}